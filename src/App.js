import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "./pages/Home.js";
import Result from "./pages/Result.js";
function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/result" element={<Result />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
